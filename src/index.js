import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Main from './components/Main';
import * as serviceWorker from './serviceWorker';
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';

export const theme = createMuiTheme({
	palette: {
		primary: { main: '#005ff9' },
		secondary: { main: '#ed254e' },
		inherit: {main: '#06908f'},
	},
});

ReactDOM.render(
	<MuiThemeProvider theme={theme}>
		<Main />
	</MuiThemeProvider>,
	document.getElementById('root')
);

serviceWorker.unregister();
