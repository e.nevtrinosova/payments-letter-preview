import React from 'react';
import InputForm from '../InputForm';
import Header from '../Header';

export default class Main extends React.Component {
	render() {
		return (
			<>
				<Header/>
				<InputForm/>
			</>
		);
	}
}
