import * as React from 'react';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import JSONInput from 'react-json-editor-ajrm';
import locale from 'react-json-editor-ajrm/locale/en';
import inputFormStyles from './styles.css';

import Bill from 'payments.letter/src/components/Billet/Bill/Bill.js';
import IncompleteOrder from 'payments.letter/src/components/Billet/IncompleteOrder/IncompleteOrder.js';

const styles = theme => ({
	skinSelect: {
		marginTop: 24,
		marginBottom: 4,
		width: 800,
	},
	enterContainer: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	textField: {
		marginLeft: theme.spacing.unit,
		marginRight: theme.spacing.unit,
		textAlign: 'left',
		width: 500,
	},
	textTitle: {
		marginLeft: theme.spacing.unit,
		marginRight: theme.spacing.unit,
	},
	wrapperContainer: {
		width: 650,
		marginBottom: 100,
	},
	button: {
		marginTop: 16,
		marginBottom: 16,
	},
	errorTitle: {
		marginTop: 16,
	},
	docLinkContainer: {
		display: 'flex',
		justifyContent: 'center',
	},
});

const MONTHS = {
	0: 'Январь',
	1: 'Ферваль',
	2: 'Март',
	3: 'Апрель',
	4: 'Май',
	5: 'Июнь',
	6: 'Июль',
	7: 'Август',
	8: 'Сентябрь',
	9: 'Октябрь',
	10: 'Ноябрь',
	11: 'Декабрь',
};

const SKINS = {
	0: 'dataset',
	1: 'promo',
};

const UIDL = '15481495260000000783';

class InputForm extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			enterText: '',
			isCorrectlyWrapperData: false,
			isParseError: false,
			paymentInfo: {},
			skin: 0,
			jsObject: {},
			errorMessage: ''
		}
	}

	handleChangeJsonEditorValue = (arg) => {
		if (arg.jsObject) {
			this.setState({jsObject: arg.jsObject});
		}
	};

	handleChangeSkin = (event, value) => {
		this.setState({
			skin: value,
		});

		this.onChangeWrapper(value);
	};

	checkForError = (paymentInfo) => {
		if (!paymentInfo) {
			return 'Вы не ввели JSONLD или выбрали не тот тип плашки.';
		}

		if (paymentInfo && !paymentInfo.amount) {
			return'Не введено обязательное поле price.';
		}

		return '';
	};

	onChangeWrapper = (skin) => {
		let paymentInfo;

		if (SKINS[skin] === 'dataset') {
			paymentInfo = this.getPaymentInfoDataset();
		}

		if (SKINS[skin] === 'promo') {
			paymentInfo = this.getPaymentInfoPromo();
		}

		const errorMessage = this.checkForError(paymentInfo);

		if (errorMessage.length === 0) {
			this.setState({isCorrectlyWrapperData: true, paymentInfo: paymentInfo, isParseError: false});
		} else {
			this.setState({errorMessage: errorMessage, isCorrectlyWrapperData: false, paymentInfo: {}, isParseError: true});
		}
	};

	getPaymentInfoDataset() {
		const result = {};

		let jsonLD;
		try {
			jsonLD = this.state.jsObject;

			result.amount = +jsonLD["totalPaymentDue"]["price"];

			result.payer = jsonLD["customer"]["name"];
			result.address = jsonLD["customer"]["address"];
			result.linkReceipt = jsonLD["url"];

			const period = new Date(jsonLD["billingPeriod"]);
			result.period = `${MONTHS[period.getMonth()]} ${period.getFullYear()}`;

			result.paymentUrl = `/payments/letter/${UIDL}`;
			result.detailedUrl = `/payments/letter/${UIDL}&detailed=true`;

			result.moreInfo = true;

			return result;
		} catch (e) {
			console.log('Ошибка парсинга введенного Jsonld ', e);
			this.setState({isParseError: true});
		}
	}

	getPaymentInfoPromo() {
		const result = {};

		let jsonLD;
		try {
			jsonLD = this.state.jsObject;

			const acceptedOffer = jsonLD["acceptedOffer"];
			const itemOffered = acceptedOffer["itemOffered"];

			result.amount = +acceptedOffer["price"];
			result.previewImage = itemOffered["image"];
			result.linkReceipt = itemOffered["url"];
			result.description = itemOffered["name"];

			result.paymentUrl = `/payments/letter/${UIDL}`;
			result.detailedUrl = `/payments/letter/${UIDL}&detailed=true`;

			return result;
		} catch(e) {
			console.log('Ошибка парсинга введенного Jsonld ', e);
			this.setState({isParseError: true});
		}
	}

	componentDidMount() {
		const header = document.getElementsByClassName('PortalHeader-wrapper-0-2-4')[0];
		header.className = inputFormStyles["PortalHeader-wrapper-0-2-4"];

		const documentation = document.createElement('a');
		documentation.href = "https://mail.ru";
		documentation.innerText = "Ссылка на документацию";
		documentation.className = inputFormStyles["doc-link"];

		header.appendChild(documentation);
	}

	render() {
		const {classes} = this.props;
		const {isCorrectlyWrapperData, paymentInfo, skin, isParseError, errorMessage} = this.state;

		const {
			amount,
			period,
			payer,
			address,
			pdfLink,
			detailedUrl,
			linkReceipt,
			paymentUrl,
			previewImage,
			description
		} = paymentInfo;

		const placeholder = {
			a: 'example-value'
		};

		const isDataCorrect = isCorrectlyWrapperData && !isParseError;

		return (
			<div className={classes.enterContainer}>
					<Paper className={classes.skinSelect}>
						<Tabs
							value={skin}
							onChange={this.handleChangeSkin}
							indicatorColor="primary"
							textColor="primary"
							centered
						>
							<Tab label="Квитанция"/>
							<Tab label="Брошенная корзина"/>
						</Tabs>
					</Paper>
				<JSONInput
					id='json_input'
					placeholder={placeholder}
					locale={locale}
					theme="light_mitsuketa_tribute"
					height='400px'
					width='800px'
					colors={{
						background: "rgba(241, 241, 241, 1)"
					}}
					onChange={this.handleChangeJsonEditorValue}
				/>
				<Button
					variant="contained"
					color="primary"
					className={classes.button}
					onClick={() => this.onChangeWrapper(skin)}
				>
					Показать плашку
				</Button>

				<div className={classes.wrapperContainer}>
					{isDataCorrect && skin === 0 &&
						<Bill
							amount={amount}
							period={period}
							payer={payer}
							address={address}
							pdfLink={pdfLink}
							detailedUrl={detailedUrl}
							linkReceipt={linkReceipt}
							paymentUrl={paymentUrl}
							isFake={true}
						/>
					}

					{isDataCorrect && skin === 1 &&
						<IncompleteOrder
							amount={amount}
							detailedUrl={detailedUrl}
							linkReceipt={linkReceipt}
							paymentUrl={paymentUrl}
							previewImage={previewImage}
							description={description}
							isFake={true}
						/>
					}

					{isParseError &&
						<>
							<Typography className={classes.errorTitle} variant={"subheading"} align={"center"} color={"secondary"}>
								{errorMessage}
							</Typography>
							<div className={classes.docLinkContainer}>
								<a href="https://mail.ru" className={inputFormStyles["doc-link-blue"]}>Ссылка на документацию</a>
							</div>
						</>
					}
				</div>
			</div>
		);
	}
}

export default withStyles(styles)(InputForm);
